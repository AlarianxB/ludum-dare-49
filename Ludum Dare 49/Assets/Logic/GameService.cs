﻿using System;
using UnityEngine;

public static class GameService
{
    public static event Action<GameEventTypes, GameObject> Event;
    public static bool hasDamagePowerUp;
    public static bool hasSpeedPowerUp;
    public static bool hasHealthPowerUp;

    public static void NotifyEvent(GameEventTypes eventType, GameObject triggerObject = null)
    {
        Event?.Invoke(eventType, triggerObject);
    }
}