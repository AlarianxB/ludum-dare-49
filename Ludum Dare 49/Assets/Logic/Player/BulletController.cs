using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float bulletSpeed;

    void FixedUpdate()
    {
        MoveBullet();
    }

    private void MoveBullet()
    {
        transform.Translate(Vector2.up * (Time.fixedDeltaTime * bulletSpeed));
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            GameService.NotifyEvent(GameEventTypes.DamageEnemy, other.gameObject);
        }
        Destroy(gameObject);
    }
}
