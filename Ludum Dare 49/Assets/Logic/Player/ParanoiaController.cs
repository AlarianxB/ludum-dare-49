using System.Collections;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.UI;

public class ParanoiaController : MonoBehaviour
{
    public float minTimer;
    public float maxTimer;
    public float timeInParanoia;
    public AudioSource paranoiaSound;

    private Light2D ambientLight;
    private Image paranoiaText;
    private float paranoiaTimer;
    private bool isActive;

    private void Start()
    {
        ambientLight = GameObject.FindWithTag("AmbientLight").GetComponent<Light2D>();
        paranoiaText = GameObject.FindWithTag("ParanoiaText").GetComponent<Image>();
        SetTimer();
    }

    private void Update()
    {
        paranoiaTimer -= Time.deltaTime;

        if (paranoiaTimer <= 0 && !isActive) StartCoroutine(SetParanoia());
    }

    private void SetTimer()
    {
        paranoiaTimer = Random.Range(minTimer * 60, maxTimer * 60);
    }

    private IEnumerator SetParanoia()
    {
        isActive = true;
        ambientLight.intensity = 0.22f;
        paranoiaSound.mute = false;
        paranoiaSound.time = 0;
        StartCoroutine(ShowParanoiaText());
        GameService.NotifyEvent(GameEventTypes.ParanoiaOn);
        
        yield return new WaitForSecondsRealtime(timeInParanoia);
        
        GameService.NotifyEvent(GameEventTypes.ParanoiaOff);
        ambientLight.intensity = 1;
        isActive = false;
        SetTimer();
        paranoiaSound.mute = true;
    }

    private IEnumerator ShowParanoiaText()
    {
        Color paranoiaTextColor = paranoiaText.color;

        paranoiaTextColor.a = 0;

        while (paranoiaTextColor.a < 1)
        {
            paranoiaTextColor.a += 0.01f;
            yield return new WaitForSecondsRealtime(0.005f);
            paranoiaText.color = paranoiaTextColor;
        }
        
        yield return new WaitForSecondsRealtime(1);
        
        while (paranoiaTextColor.a > 0)
        {
            paranoiaTextColor.a -= 0.01f;
            yield return new WaitForSecondsRealtime(0.005f);
            paranoiaText.color = paranoiaTextColor;
        }
    }
}