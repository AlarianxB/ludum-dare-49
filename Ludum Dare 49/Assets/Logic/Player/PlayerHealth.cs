using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using Vector2 = UnityEngine.Vector2;

public class PlayerHealth : MonoBehaviour
{
    public GameObject healthUI;
    public AudioSource bellSource;
    public AudioClip bellSound;
    public bool bellPlayed;

    private int health = 100;
    private int maxHealth = 100;
    private bool hasParanoia;
    private Slider healthSlider;
    private RectTransform healthTransform;

    private Vector2 normalSize;
    private Vector2 powerUpSize;

    void Start()
    {
        GameService.Event += OnGameEvent;
        healthSlider = healthUI.GetComponent<Slider>();
        healthTransform = healthUI.GetComponent<RectTransform>();

        Vector2 sizeDelta = healthTransform.sizeDelta;
        normalSize = new Vector2(230, sizeDelta.y);
        powerUpSize = new Vector2(345, sizeDelta.y);
    }

    // Update is called once per frame
    void Update()
    {
        healthSlider.value = health;

        if (health <= 0)
        {
            GameService.NotifyEvent(GameEventTypes.PlayerDied);
            if (!bellPlayed)
            {
                bellSource.PlayOneShot(bellSound, 0.3f);
                bellPlayed = true;
            }
        }
        
    }

    private void OnDestroy()
    {
        GameService.Event -= OnGameEvent;
    }

    private void OnGameEvent(GameEventTypes eventType, GameObject triggerObject = null)
    {
        switch (eventType)
        {
            case GameEventTypes.DamagePlayer:
                health -= hasParanoia ? 10 : 20;
                break;
            case GameEventTypes.DamagePlayerAsBoss:
                health -= hasParanoia ? 15 : 30;
                break;
            case GameEventTypes.ParanoiaOn:
                hasParanoia = true;
                health = maxHealth;
                break;
            case GameEventTypes.ParanoiaOff:
                hasParanoia = false;
                health = maxHealth / 2;
                break;
            case GameEventTypes.MaxHealth:
                maxHealth = 150;
                healthSlider.maxValue = maxHealth;
                healthTransform.sizeDelta = powerUpSize;
                health = maxHealth;
                break;
        }
    }
}
