using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public float movementSpeed;
    public float powerUpSpeed;
    public AudioSource footsteps;
    
    private PlayerActions playerInput;
    private Rigidbody2D playerRigidbody;
    private float speed;
    public Animator playerAnimator;
    private static readonly int Walk = Animator.StringToHash("Walk");

    private void Awake()
    {
        playerInput = new PlayerActions();
        playerRigidbody = gameObject.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Move();
        Look();
    }

    private void Update()
    {
        speed = GameService.hasSpeedPowerUp ? powerUpSpeed : movementSpeed;
    }
    private void Move()
    {
        playerRigidbody.MovePosition(playerRigidbody.position + GetDirectionVector() * (speed * Time.deltaTime));

        playerAnimator.SetBool(Walk, !(GetDirectionVector().magnitude <= 0));

        footsteps.volume = playerAnimator.GetBool(Walk) ? 0.6f : 0;
    }

    private void Look()
    {
        Vector3 mousePosition = playerInput.Movement.Look.ReadValue<Vector2>();
        if (Camera.main is null) return;
        Vector3 dir = mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }

    private void OnEnable()
    {
        playerInput.Enable();
    }

    private void OnDisable()
    {
        playerInput.Disable();
    }

    private Vector2 GetDirectionVector()
    {
        float verticalInput = playerInput.Movement.Directional.ReadValue<Vector2>().y;
        float horizontalInput = playerInput.Movement.Directional.ReadValue<Vector2>().x;

        return new Vector2(horizontalInput, verticalInput);
    }
}
