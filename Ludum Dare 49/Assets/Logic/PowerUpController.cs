using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpController : MonoBehaviour
{
    public enum PowerUp
    {
        Damage,
        Speed,
        Health
    }

    public PowerUp powerUpType;

    private Image sprite;
    void Start()
    {
        GetImage();
        GameService.hasDamagePowerUp = false;
        GameService.hasSpeedPowerUp = false;
        GameService.hasHealthPowerUp = false;
    }

    private void GetImage()
    {
        sprite = GameObject.FindGameObjectWithTag(powerUpType.ToString()).GetComponent<Image>();
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        sprite.enabled = true;

        switch (powerUpType)
        {
            case PowerUp.Damage:
                GameService.hasDamagePowerUp = true;
                break;
            case PowerUp.Speed:
                GameService.hasSpeedPowerUp = true;
                break;
            case PowerUp.Health:
                GameService.hasHealthPowerUp = true;
                GameService.NotifyEvent(GameEventTypes.MaxHealth);
                break;
        }
        Destroy(gameObject);
    }
}
