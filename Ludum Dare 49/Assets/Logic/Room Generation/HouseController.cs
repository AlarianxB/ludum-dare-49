using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HouseController : MonoBehaviour
{
    public List<GameObject> rooms;

    public GameObject boss;

    void Start()
    {
        // GetAllRooms();
        // OpenAllDoors();

    }

    private void GetAllRooms()
    {
        rooms = new List<GameObject>();
        GameObject house = GameObject.FindWithTag("Rooms");
        foreach (Transform room in house.transform)
        {
            rooms.Add(room.gameObject);
        }
    }


    private void OpenAllDoors()
    {
        foreach (GameObject doors in rooms.Select(room => room.GetComponent<RoomController>().doors).Where(doors => doors != null))
        {
            doors.SetActive(false);
        }
    }

}
