﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoomController : MonoBehaviour
{
    public GameObject doors;
    public GameObject spawnPoints;
    public List<GameObject> enemies;
    public GameObject boss;
    public bool isEntryRoom;
    public bool hasNoEnemies;
    public bool isBossRoom;

    private List<GameObject> spawnPointsList;
    private int currentWave;
    private int enemyCount;
    private bool isActive;
    private bool wasVisited;

    private void Start()
    {
        if (isEntryRoom) return;
        OpenDoors();
        if (hasNoEnemies) return;
        CreateSpawnPointsList();
        GameService.Event += OnGameEvent;
    }
    
    private void Update()
    {
        if (hasNoEnemies) return;
        if (currentWave < 2 || enemyCount > 0) return;
        OpenDoors();
        isActive = false;
    }

    private void OnDestroy()
    {
        if (isEntryRoom || hasNoEnemies) return;
        GameService.Event -= OnGameEvent;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player") || wasVisited || hasNoEnemies) return;
        CloseDoors();
        SpawnEnemies();
        isActive = true;
        wasVisited = true;
    }

    private void CloseDoors()
    {
        doors.SetActive(true);
    }
    
    private void OpenDoors()
    {
        doors.SetActive(false);
    }

    private void CreateSpawnPointsList()
    {
        spawnPointsList = new List<GameObject>();
        if (isBossRoom)
        {
            spawnPointsList.Add(gameObject);
        }
        else
        {
            for (int i = 0; i < spawnPoints.transform.childCount; i++)
            {
                spawnPointsList.Add(spawnPoints.transform.GetChild(i).gameObject);
            }
        }

    }

    private void SpawnEnemies()
    {
        if (isBossRoom)
        {
            enemyCount = 1;
            currentWave = 2;
            
            GameObject enemy = Instantiate(boss, spawnPointsList[0].transform.position, Quaternion.identity);
            Vector3 position = enemy.transform.position;
            position = new Vector3(position.x, position.y, -5f);
            enemy.transform.position = position;
        }
        else
        {
            currentWave++;
            enemyCount = spawnPointsList.Count;

            foreach (GameObject spawnPoint in spawnPointsList)
            { 
                int enemyIndex = Random.Range(0, enemies.Count);

                GameObject enemy = Instantiate(enemies[enemyIndex], spawnPoint.transform.position, Quaternion.identity);
                Vector3 position = enemy.transform.position;
                position = new Vector3(position.x, position.y, -5f);
                enemy.transform.position = position;
            }
        }

    }
    
    private void OnGameEvent(GameEventTypes eventType, GameObject targetGameObject = null)
    {
        if (eventType != GameEventTypes.EnemyDied || !isActive) return;
        enemyCount--;

        if (enemyCount <= 0 && currentWave <= 1)
        {
            SpawnEnemies();
        }

    }

}