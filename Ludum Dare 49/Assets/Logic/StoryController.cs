using System.Collections;
using System.Collections.Generic;
using Logic;
using UnityEngine;
using UnityEngine.UI;

public class StoryController : MonoBehaviour
{
    public List<StoryTemplate> stories;
    public List<GameObject> buttons;
    public Text textArea; 
    void Start()
    {
        for (int i = 1; i <= PlayerPrefs.GetInt("Story"); i++)
        {
            buttons[i - 1].SetActive(true);
        }
    }

    public void SetStoryOnClick(int index)
    {
        textArea.text = stories[index].text;
    }
}
