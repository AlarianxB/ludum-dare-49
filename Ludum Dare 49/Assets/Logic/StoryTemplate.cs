using UnityEngine;

namespace Logic
{
    [CreateAssetMenu(fileName = "Story", menuName = "Story", order = 0)]
    public class StoryTemplate : ScriptableObject
    {
        [TextArea(10, 100)]
        public string text;
    }
}