using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class UiController : MonoBehaviour
{
    private static bool isGamePaused;
    private PlayerActions inputActions;
    public GameObject pauseMenu;
    public GameObject youDiedMenu;
    public Canvas homeCanvas;
    public Canvas storyInitialCanvas;
    public Canvas storyCanvas;

    private void Awake()
    {
        inputActions = new PlayerActions();
        inputActions.UI.PauseGame.performed += _ => SetPause();
        
        if (!PlayerPrefs.HasKey("Story"))
        {
            PlayerPrefs.SetInt("Story", 1);
            PlayerPrefs.Save();
        }
    }

    private void Start()
    {
        GameService.Event += ShowDeathScreen;
        Time.timeScale = 1;
        isGamePaused = false;
    }

    private void OnDestroy()
    {
        GameService.Event -= ShowDeathScreen;
    }

    private void ShowDeathScreen(GameEventTypes eventType, GameObject targetObject = null)
    {
        if (eventType != GameEventTypes.PlayerDied) return;
        youDiedMenu.SetActive(true);
        Time.timeScale = 0;
        isGamePaused = true;
    }

    private void OnEnable()
    {
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }


    private void SetPause()
    {
        if (isGamePaused)
        {
            ResumeGame();
        }
        else
        {
            PauseGame();
        }
    }

    public void StartGame()
    {
        Time.timeScale = 1;
        isGamePaused = false;
        int houseIndex = Random.Range(1, 6);
        SceneManager.LoadScene(houseIndex);
    }

    public void PauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        isGamePaused = true;
    }

    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        isGamePaused = false;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Main_Menu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ShowStoryInitial()
    {
        storyInitialCanvas.enabled = true;
        homeCanvas.enabled = false;
        storyCanvas.enabled = false;
    }
    
    public void ShowStory()
    {
        storyInitialCanvas.enabled = false;
        homeCanvas.enabled = false;
        storyCanvas.enabled = true;
    }
}
